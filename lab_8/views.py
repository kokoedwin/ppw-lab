from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
response = {}
author = "Jonathan Edwin"

def index(request):
    response = {"author": author}
    html = 'lab8.html'
    return render(request, html, response)
